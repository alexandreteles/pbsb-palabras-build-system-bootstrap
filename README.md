# PBSB: Palabras build system bootstrap
### A bunch of scripts to bootstrap the build system needed to pull and compile the palaBras software using Docker containers (or any other software of your choose)

#### Please, go to the wiki if you have any questions about this software.

## Releases:
### Latest

* PBSB v0.2.1 (Athanasius) - [Download](https://bitbucket.org/Kabbalista/pbsb-palabras-build-system-bootstrap/get/7c5528a.zip)

### Old releases:

* PBSB v0.2 (Athanasius) - [Download](https://bitbucket.org/Kabbalista/pbsb-palabras-build-system-bootstrap/get/fc7779f.zip)
* PBSB_v0.1 (Ambrosius) - [Download](https://bitbucket.org/Kabbalista/pbsb-palabras-build-system-bootstrap/get/b9bbb4f.zip)

