#!/bin/bash

#############################
#  Author: Alexandre Teles  #
# Email: ateles.1@gmail.com #
#       On: 14/04/2014      #
#       License: GPLv3      #
#############################

# Test if are root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
# Initial notices
printf "This script will install the lateste version of Docker on this 
system.\n Run this only once, in a clean install.\n"
# Start the system update procedure
printf "Updating system packages...\n"
apt-get update; apt-get upgrade; apt-get dist-upgrade
# Try to install dependencies
printf "Installing dependencies...\n"
[ -e /usr/lib/apt/methods/https ] || {
  apt-get update
  apt-get install apt-transport-https
}
# Adding docker repository to APT sources
echo deb https://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list
# Adding docker repository key
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
# Installing docker
apt-get update; apt-get install -y lxc-docker
# Test docker installation
docker run -it ubuntu printf "Installation successeful. Returning...\n"
