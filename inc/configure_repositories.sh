#!/bin/bash

#############################
#  Author: Alexandre Teles  #
# Email: ateles.1@gmail.com #
#       On: 14/04/2014      #
#       License: GPLv3      #
#############################

# Test if are root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
# This script will configure the sources.list file to reflect what is needed to run the palaBras build system
printf "Backing up original sources.list file.\n"
mv /etc/apt/sources.list /etc/apt/sources.list.0
printf "Installing new sources.list file.\n"
cp ../config/sources.list /etc/apt/sources.list
printf "All done.\n"
