#!/bin/bash

#############################
#  Author: Alexandre Teles  #
# Email: ateles.1@gmail.com #
#       On: 16/04/2014      #
#       License: GPLv3      #
#############################

# Test if are root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
printf "Installing Docker nonroot commands...\n"
# Copy the scripts that grant nonroot access to Docker
cp ../bin/* /usr/bin
printf "Installing complete.\n"