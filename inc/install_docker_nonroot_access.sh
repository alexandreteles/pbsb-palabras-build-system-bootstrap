#!/bin/bash

#############################
#  Author: Alexandre Teles  #
# Email: ateles.1@gmail.com #
#       On: 16/04/2014      #
#       License: GPLv3      #
#############################

# Test if are root
if [[ $EUID -ne 0 ]]; then
   printf "This script must be run as root.\n" 1>&2
   exit 1
fi
# Check if the group exists
if id -g docker >/dev/null 2>&1; then
        printf "Group already exists. Aborting...\n" 1>&2
        exit 1
fi
# Add the docker group if it doesn't already exist.
printf "Adding docker group...\n"
groupadd docker
# Restart the Docker daemon.
printf "Restarting docker daemon...\n"
service docker restart
# Install Docker custom commands
./install_commands.sh