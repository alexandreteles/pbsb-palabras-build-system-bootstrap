#!/bin/bash

#############################
#  Author: Alexandre Teles  #
# Email: ateles.1@gmail.com #
#       On: 14/04/2014      #
#       License: GPLv3      #
#    Based on: CentMin Mod  #
#############################

# Test if are root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
printf "Installing CSF firewall...\n"
# Get actual directory
initialdir=$PWD
cd /tmp
# Install dependencies
apt-get -y install sysv-rc-conf libwww-perl
# Download csf tarball
wget http://configserver.com/free/csf.tgz
# Extract csf sources
tar xzf csf.tgz
cd csf
# Install CSF
sh install.sh
# Returning to initial dir
cd $initialdir
# Now we need to test IPTables modules to grant the best compatibility
printf "Test IP Tables Modules...\n"
perl /etc/csf/csftest.pl
# Here we need to set all port that are required to full PBS functionality
printf "CSF adding PBS ports to csf.allow list...\n"
sed -i 's/20,21,22,25,53,80,110,143,443,465,587,993,995/20,21,22,25,53,80,110,111,143,161,443,465,587,993,995,1110,1186,1194,2202,11211,11212,11213,11214,2049,2112,22000,22001,2222,3334,8080,8888,81,9000,9001,9312,9418,10000,10500,10501,6081,6082,30865,3000:3050/g' /etc/csf/csf.conf
sed -i "s/TCP_OUT = \"/TCP_OUT = \"111,2049,1110,1194,9418,/g" /etc/csf/csf.conf # CHANGE PORTS TO REFLECT PBS REQUIREMENTS!!!
sed -i "s/UDP_IN = \"/UDP_IN = \"111,2049,1110,/g" /etc/csf/csf.conf # CHANGE PORTS TO REFLECT PBS REQUIREMENTS!!!
sed -i "s/UDP_OUT = \"/UDP_OUT = \"111,2049,1110,/g" /etc/csf/csf.conf # CHANGE PORTS TO REFLECT PBS REQUIREMENTS!!!
# In order to firewall really protect the system we need to disable the CSF Testing Mode
printf "Disabling CSF Testing mode (activates firewall)...\n"
sed -i 's/TESTING = "1"/TESTING = "0"/g' /etc/csf/csf.conf
# Now we have to tweak some CSF options
./tweak_csf.sh
# Check to see if csf.pignore already has custom apps added
CSFPIGNORECHECK=`grep -E '(user:nginx|user:nsd|exe:/usr/local/bin/memcached)' /etc/csf/csf.pignore`
if [[ -z $CSFPIGNORECHECK ]]; then
printf "Adding applications and users to CSF ignore list...\n"
cat >>/etc/csf/csf.pignore<<EOF
user:mysql
exe:/usr/sbin/mysqld
cmd:/usr/sbin/mysqld
exe:/usr/libexec/hald-addon-acpi
cmd:hald-addon-acpi
user:dbus
user:postfix
user:www-data
exe:/usr/libexec/postfix/smtp
exe:/usr/libexec/postfix/smtpd
exe:/usr/libexec/postfix/pickup
exe:/usr/libexec/postfix/tlsmgr
exe:/usr/libexec/postfix/qmgr
exe:/usr/libexec/postfix/virtual
exe:/usr/libexec/postfix/proxymap
exe:/usr/libexec/postfix/anvil
exe:/usr/libexec/postfix/lmtp
exe:/usr/libexec/postfix/scache
exe:/usr/libexec/postfix/cleanup
exe:/usr/libexec/postfix/trivial-rewrite
exe:/usr/libexec/postfix/master
exe:/usr/bin/docker
EOF
fi
# Adding CSF to system upstart
sysv-rc-conf csf on
service csf restart
csf -r
sysv-rc-conf lfd on
service lfd start
printf "CSF firewall successfully installed.\n"
