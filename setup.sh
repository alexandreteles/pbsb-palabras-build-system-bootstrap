#!/bin/bash

#############################
#  Author: Alexandre Teles  #
# Email: ateles.1@gmail.com #
#       On: 14/04/2014      #
#       License: GPLv3      #
#############################

# Test if are root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
printf "This script will bootstrap all what are needed to build the palaBras application\n"
sleep 2s
printf "\nAltough not really recomended, you can use this script to bootstrap a general build system.\n"
printf "If you want to do this, you will need to setup your build dependencies by hand on Docker Containers. Good luck :D\n"
sleep 8s
printf "\nFinally, if you want to stop the installation, please,\n"
printf "type CTRL+C in the next ten seconds or just wait to continue.\n"
sleep 10s
printf "\nStarting installation...\n"
# Setting up repositories
./inc/configure_repositories.sh
# Installing CSF firewall
./inc/install_csf.sh
# Installing Docker
./inc/install_docker.sh
# Installing docker nonroot access
./inc/install_docker_nonroot_access.sh
printf "\nInstalling complete.\n"
printf "\nAfter this installation, run docker-add-nonroot <user> to give nonroot access to Docker to the user that you want.\n"
printf "\nGoodbye.\n"
