# What we need to do

## Create script to install and configure CSF (100%);
## Create script to install and configure Docker (100%);
## Create dockerfiles for build images [Fedora, Ubuntu and CentOS]
## Create a install script that can:
### Create all containers at install time (depends on: create dockerfiles for build images);
### Create swap directory for package repository;
### Create repositories at install time (depends on: create dockerfile for repository images);
### Create pack routine to continuously create backups of containers;
## Create a script to install TJWS and mini_httpd and:
### Deploy Jenkins Continuos Integration System;
### Deploy a GetSimple blog that:
#### Can pull releases from repositories;
#### Automatically update nightly builds;
#### Automaticaly update stables and testing builds in the release.
